<?php

use App\Models\Category;
use App\Models\Post;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('id',1)->get();
        $categories = Category::get()->pluck('id')->toArray();
        for ( $i=1; $i<=8; $i++ )
        {
            $faker = Faker\Factory::create();



            $postData =
            [
                'title'                 => $faker->sentence(),
                'content'               => $faker->paragraph(10,true),
                'slug'                  => $faker->slug(),
                'status'                => 1,
                'user_id'               => $user[0]->id,
            ];


            $post = Post::create($postData);

            $postCategories = Arr::random($categories,2);
            $post->categories()->sync($postCategories);


        }
    }
}
