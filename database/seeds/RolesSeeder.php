<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Guard;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $roles =
        [
            ['name' => 'manager', 'created_at' => $now, 'updated_at' => $now,'guard_name' =>Guard::getDefaultName(static::class)],
            ['name' => 'member','created_at' => $now, 'updated_at' => $now,'guard_name' =>Guard::getDefaultName(static::class)],
        ];
        Role::insert($roles);
    }
}
