<?php

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $roles =
        [
            ['name' => 'sport', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'news','created_at' => $now, 'updated_at' => $now],
            ['name' => 'game','created_at' => $now, 'updated_at' => $now],
            ['name' => 'ITC','created_at' => $now, 'updated_at' => $now],
            ['name' => 'plumbing','created_at' => $now, 'updated_at' => $now],
            ['name' => 'construction','created_at' => $now, 'updated_at' => $now],
        ];
        Category::insert($roles);
    }
}
