<?php

use Spatie\Permission\Models\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name','manager')->first();
        $userData =
        [
            'name' => 'Angelo',
            'email' => 'angelosottile@email.it',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'), //così si generano le password di Laravel
            'remember_token' => Str::random(10),
            'slug'  =>uniqid(),
        ];

        $newUser = User::Create($userData);

        $newUser->syncRoles($adminRole);
        $newUser->save();
    }
}
