@extends('layouts.app')

@section('content')
 <div class="container col-md-10 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Create a new post</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <form method="post" action="{{ route('user.store') }}" enctype="multipart/form-data">
                    @csrf

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @include('components.message')

                    <div class="form-group">
                        <label for="title" class="col-lg-12 control-label">Title</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{old('title') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="content" class="col-lg-12 control-label">Content</label>
                        <div class="col-lg-12">
                            <textarea class="form-control" rows="3" id="content" name="content" value="{{old('content') }}"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="categories" class="col-lg-12 control-label">Categories</label>
                        <div class="col-lg-12">
                            <select class="form-control" id="category" name="categories[]" multiple>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="file" name="image" class="form-control">
                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
