@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="mx-auto col-lg-8 col-md-10">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @include('components.message')

            @foreach($posts as $post)
                <div class="post-preview">

                    <a href=" {{ route('user.show',$post) }} ">
                        <h2 class="post-title">{{$post->title}}</h2>
                        <h3 class="post-subtitle"> {{Str::limit($post->content,100)}} </h3>
                    </a>

                    @foreach ($post->comments as $comment )
                        <h6 class="post-subtitle"> {{ $comment->content }} </h6>
                    @endforeach


                    @foreach($post->images as $image)
                        <img src="{{ Storage::url($image->title) }}" class="img-fluid" style="width: 100%;height:300px">
                    @endforeach

                    <p class="post-meta">Posted by  <a href="#">{{$post->user->name}}</a> on {{$post->created_at->format('d-m-Y')}}
                        Categories:
                        @foreach($post->categories as $category)
                            {{$category->name}}
                        @endforeach
                    </p>
                </div>
                <hr>
            @endforeach


            <div class="clearfix">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>

  <hr>
@endsection
