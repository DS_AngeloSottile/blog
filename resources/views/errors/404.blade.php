@extends('layouts.app')
@section('title', 'Error')
@section('content')

    <header class="masthead" style="background-image: url({{url('/img/post-bg.jpg')}})">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="page-heading">
                        <h1>Non è stato possibile accedere a questa risorsa</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

@endsection
