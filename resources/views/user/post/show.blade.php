@extends('layouts.app')
@section('title', $post->title)
@section('content')

    <header class="masthead" style="background-image: url({{url('/img/post-bg.jpg')}})">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="page-heading">
                        <h1>{{$post->title}}</h1>
                        <span class="subheading">
                            <p class="post-meta">Posted by <a style="color:white" href="#">{{$post->user->name}}</a> on {{$post->created_at->format('d-m-Y')}}
                                Categories:
                                @foreach($post->categories as $category)
                                    {{$category->name}}
                                @endforeach
                            </p>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <div class="container">

            <div class="col-lg-8 col-md-10 mx-auto text-center">
                <h2> Il mio post</h2>

                <div class="list-group-item">
                    <div class="row-content">
                        <h5><i class="fas fa-file-signature mb-3"></i> Manage Posts</h5>
                        <a href="{{ route('userSection.posts.edit',$post) }}" class="btn btn-info">Modifica</a>

                        <form  style="display: inline-block" method="POST" action="{{ route('userSection.posts.destroy',$post) }}">
                            @csrf
                            @method('delete')

                            <button type="submit"  class="btn btn-success">
                                Elimina
                            </button>
                        </form>

                    </div>
                </div>

                <div class="card-body">

                    @foreach($post->images as $image)
                        <img src="{{ Storage::url($image->title) }}" class="img-fluid" style="width: 100%;height:300px">
                    @endforeach
                    <p>{{ $post->content }}</p>

                </div>
                <div class="clearfix"></div>
            </div>

            @foreach($post->comments as $comment)
                <div class="col-lg-8 col-md-10 mx-auto card mb-4">
                    <div class="card-body">
                        <blockquote>{{ $comment->content }}</blockquote>
                    </div>
                </div>
            @endforeach

            <div class="col-lg-8 col-md-10 mx-auto card">
                <div class="card-body">
                    <form method="post" action="{{ route('user.commentsStore',$post) }}">
                        @csrf

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @include('components.message')

                        <div class="form-group">
                            <legend>Comment</legend>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="3" id="content" name="content" value="{{ old('content') }}"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary">Post</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection
