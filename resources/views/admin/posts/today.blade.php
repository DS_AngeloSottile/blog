@extends('layouts.app')

@section('content')
 <div class="container col-md-10 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header">
                <h5 class="float-left">All posts of today</h5>
                @include('admin.components.redirect')
                <div class="clearfix"></div>
            </div>
            <div class="content">

                <form action="{{ route('admin.selectDayPosts') }}" method="post">
                    @csrf

                    <div class="form-group">

                        <div class="col-lg-10 col-lg-offset-2">
                            <input type="date" name="dayPost" id="">

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Cateogires Number</th>
                                <th>Comments Number</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>  {{ $post->title }}  </td>
                                    <td>{{ $post->slug }}</td>
                                    <td><span class="badge"> {{ $post->categories_count}}</span></td>
                                    <td><span class="badge"> {{ $post->comments_count}}</span></td>
                                    <td>{{ $post->created_at }}</td>
                                    <td>{{ $post->updated_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
				</div>
        </div>
    </div>
@endsection
