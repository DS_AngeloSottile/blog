@extends('layouts.app')

@section('content')
 <div class="container col-md-10 col-md-offset-2">
        <div class="mt-5 card">
            <div class="card-header">
                <h5 class="float-left">All posts</h5>
                @include('admin.components.redirect')
                <div class="clearfix"></div>
            </div>
            <div class="content">

                @include('components.message')

                @if ($posts->isEmpty())
                    <p> There is no post.</p>
                @else
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>

                                        @foreach($post->images as $image)
                                            <img src="{{ Storage::url($image->title) }}" class="img-fluid" style="width: 150px;height:100%">
                                        @endforeach

                                    </td>
                                    <td>
                                        <a href="{{ route('admin.posts.edit',$post) }}">{{ $post->title }} </a>
                                    </td>
                                    <td>{{ $post->slug }}</td>
                                    <td>{{ $post->created_at }}</td>
                                    <td>{{ $post->updated_at }}</td>
                                    <td>
                                        <form  style="display: inline-block" method="POST" action="{{ route('admin.posts.destroy',$post) }}">
                                            @csrf
                                            @method('delete')

                                            <button type="submit"  class="">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
				</div>
        </div>
    </div>
@endsection
