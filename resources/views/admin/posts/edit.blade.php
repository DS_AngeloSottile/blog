@extends('layouts.app')

@section('content')
  <div class="container col-md-10 col-md-offset-2">
        <div class="mt-5 card">
            <div class="card-header ">
                <h5 class="float-left">Edit a post</h5>
                @include('admin.components.redirect')
                <div class="clearfix"></div>
            </div>
            <div class="mt-2 card-body">
                <form method="post" action="{{ route('admin.posts.update',$post) }}"  enctype="multipart/form-data">
                    @csrf
                    @method('patch')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @include('components.message')


                    <div class="form-group">
                        <label for="title" class="col-lg-12 control-label">Title</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{old('title') ? old('title') : $post->title}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="content" class="col-lg-12 control-label">Content</label>
                        <div class="col-lg-12">
                            <textarea class="form-control" rows="3" id="content" name="content" value="{{old('content') ? old('content') : $post->content}}">{{ $post->content }}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="categories" class="col-lg-12 control-label">Categories</label>

                        <div class="col-lg-12">
                            <select class="form-control" id="category" name="categories[]" multiple>

                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"  @if(in_array($category->id, $selectedCategories))
                                    selected="selected" style="background-color: green;" @endif >{{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12" style="background-color: lightblue">

                        @if ( count($post->images ) > 0)

                            @foreach($post->images as $image)
                                <img src="{{ Storage::url($image->title) }}" class="img-fluid" style="width: 50%;height:300px">
                                <div class="form-group">
                                    Cambia Immagine
                                    <input type="file" name="imageEdit" class="form-control">
                                </div>
                            @endforeach

                        @else

                            <div class="form-group">
                                Aggiungi Immagine
                                <input type="file" name="imageAdd" class="form-control">
                            </div>

                        @endif



                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="reset" class="btn btn-default">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
