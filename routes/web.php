<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::auth();
Route::get('/', 'PageController@index')->name('home');
Route::get('/contact', 'PageController@contact')->name('contact');

Route::get('/admin', 'PageController@adminHome')->middleware('manager')->name('admin');



Route::prefix('admin-users-managment')->namespace('Admin')->name('admin.')->middleware('admin')->group( function () // ->middleware('manager') php artisan make:middleware manager
{
    Route::resource('users', 'UsersController');
});


Route::prefix('roles-managment')->namespace('Admin')->name('admin.')->middleware('manager')->group( function () //php artisan make:middleware manager
{
    Route::resource('roles', 'RolesController');
});


Route::prefix('admin-posts-managment')->namespace('Admin')->name('admin.')->middleware('manager')->group( function ()
{
    Route::resource('posts', 'PostsController');
    Route::get('/todayPosts','PostsController@getToday')->name('todayPosts');
    Route::post('/selectDayPosts','PostsController@selectDayPosts')->name('selectDayPosts');
});


Route::prefix('admin-categories-managment')->namespace('Admin')->name('admin.')->middleware('manager')->group( function ()
{
    Route::resource('categories', 'CategoryController');
});


Route::prefix('user-commentsAndPost')->namespace('User')->name('user.')->middleware('auth')->group( function ()
{
    Route::get('/create', 'PostsController@create')->name('create');
    Route::post('/store', 'PostsController@store')->name('store');
    Route::get('/show/{post}', 'PostsController@show')->middleware('auth')->name('show');
    Route::post('/commentsStore/{post}', 'CommentsController@store')->name('commentsStore');

});


Route::prefix('userSection')->namespace('User')->name('userSection.')->middleware('auth')->group( function ()
{
    Route::resource('posts','UserPostController');
});
