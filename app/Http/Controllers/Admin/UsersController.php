<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditFormRequest;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        if(!$user)
        {
            $errors =
            [
                'error_message' => 'User non trovato'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }

        $roles = Role::all();
        $selectedRoles = $user->roles()->pluck('name')->toArray();

        return view('admin.user.edit',compact('user','roles','selectedRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditFormRequest $request, User $user)
    {
        try
        {
            DB::beginTransaction();

                $user->name = $request->name;
                $user->email = $request->email;

                if($request->password != null)
                {
                    $user->password = Hash::make($request->password);
                }

                $user->syncRoles($request->role); //syncRoles rimuove tutti i ruoli inseriti e aggiunge quello appena inserito
                $user->save();

            DB::commit();

                session()->flash('success',('Utente mofificato con successo'));
                return redirect()->route('admin.users.edit',$user);
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nella modifica del utente',
            ];
            session()->flash('error',($errors));
            return redirect()->route('admin.users.edit',$user);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
