<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostEditRequest;
use App\Http\Requests\PostFormRequest;
use App\Models\Category;
use App\Models\Image;
use App\Models\Post;
use App\Traits\ImgManageTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostsController extends Controller
{
    use ImgManageTrait;

    public function getToday()
    {

        $posts = Post::selectDay(Carbon::today());

        return view('admin.posts.today',compact('posts'));
    }

    public function selectDayPosts(Request $request)
    {
        $day = $request->dayPost;

        $carbonDate = Carbon::parse($day);

        $posts = Post::selectDay($carbonDate);

        return view('admin.posts.today',compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('categories')->get();

        return view('admin.posts.index',compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {

        if(!$post)
        {
            $errors =
            [
                'error_message' => 'Post non trovato'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }

        $categories = Category::all();
        $selectedCategories = $post->categories->pluck('id')->toArray();
        return view('admin.posts.edit',compact('post','categories','selectedCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostEditRequest $request, Post $post)
    {

        try
        {
            DB::beginTransaction();

                $post->title = $request->title;
                $post->content = $request->content;


                $post->categories()->sync($request->categories);

                if($request->hasFile('imageEdit'))
                {

                    $image = Image::where('post_id',$post->id)->firstOrFail();
                    $this->editImage($request->imageEdit,$post,$image );
                }

                if($request->hasFile('imageAdd'))
                {
                    $this->addImage($request->imageAdd, $post);
                }

                $updated = $post->save();

            DB::commit();

                session()->flash('success',('Post mofificato con successo'));
                return redirect()->route('admin.posts.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nella modifica del post',
            ];
            session()->flash('error',($errors));
            return redirect()->route('admin.posts.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try
        {
            DB::beginTransaction();

                foreach($post->images as $image)
                {
                    if(file_exists( public_path('storage/'.$image->title) ) )
                    {
                        unlink(public_path('storage/'.$image->title));
                    }
                }
                Image::where('post_id',$post->id)->delete();
                $deleted = $post->delete();


            DB::commit();

                session()->flash('success',('Post cancellato con successo'));
                return redirect()->route('admin.posts.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nella rimozione del post',
            ];
            session()->flash('error',($errors));
            return redirect()->route('admin.posts.index');
        }

    }
}
