<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostFormRequest;
use App\Models\Category;
use App\Models\Image;
use App\Models\Post;
use App\Traits\ImgManageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Unique;

class PostsController extends Controller
{

    use ImgManageTrait;


    public function create()
    {
        try
        {
            DB::beginTransaction();

                $categories = Category::all();

            DB::commit();

                return view('post.create',compact('categories'));
        }
        catch(\Exception $e)
        {

            $errors =
            [
                'error_message' => 'Errore nella creazione del post',
            ];
            session()->flash('error',($errors));
            return redirect()->route('home');
        }
    }


    public function store(PostFormRequest $request)
    {

        try
        {
            DB::beginTransaction();

                $user = Auth::user();

                $post = $user->posts()->create(
                [
                    'title'     => $request->title,
                    'content'   => $request->content,
                    'slug'      => uniqid(),
                ]);

                $post->categories()->sync($request->categories);

                if($request->hasFile('image'))
                {
                    $this->addImage($request->image, $post);
                }

            DB::commit();

                session()->flash('success',('Post creato con successo'));
                return redirect()->route('home');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nella creazione del post',
            ];
            session()->flash('error',($errors));
            return redirect()->route('home');
        }

    }


    public function show( Post $post)
    {

        if(!$post)
        {
            $errors =
            [
                'error_message' => 'Post non trovato'
            ];
            session()->flash('error',$errors);
            return redirect()->route('home');
        }

        return view('post.show',compact('post'));
    }
}
