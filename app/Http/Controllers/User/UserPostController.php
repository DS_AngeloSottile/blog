<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostEditRequest;
use App\Models\Category;
use App\Models\Image;
use App\Models\Post;
use App\Traits\ImgManageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserPostController extends Controller
{
    use ImgManageTrait;


    public function index()
    {
        $user = Auth::user();

        $posts = $user->posts()->paginate(3);

        return view('user.index',compact('user','posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

        if(!($post && $post->check_belong))
        {
            return redirect()->route('home');
        }

        return view('user.post.show',compact('post'));
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if(!($post && $post->check_belong))
        {
            return redirect()->back();
        }

        $categories = Category::all();
        $selectedCategories = $post->categories->pluck('id')->toArray();
        return view('user.post.edit',compact('post','categories','selectedCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostEditRequest $request, Post $post)
    {

        try
        {
            DB::beginTransaction();

            if(!$post->check_belong)
            {
                return redirect()->route('userSection.posts.index');
            }
                $post->title = $request->title;
                $post->content = $request->content;


                $post->categories()->sync($request->categories);

                if($request->hasFile('imageEdit'))
                {
                    $image = Image::where('post_id',$post->id)->firstOrFail();
                    $this->editImage($request->imageEdit,$post,$image );
                }

                if($request->hasFile('imageAdd'))
                {
                    $this->addImage($request->imageAdd, $post);
                }

                $updated = $post->save();

            DB::commit();

                session()->flash('success',('Post mofificato con successo'));
                return redirect()->route('userSection.posts.index');
        }
        catch(\Exception $e)
        {
            if(!$post->check_belong)
            {
                $errors =
                [
                    'error_message' => 'Errore nella modifica del post'
                ];
                session()->flash('error',($errors));
                return redirect()->route('userSection.posts.index');
            }
            return redirect()->route('userSection.posts.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try
        {
            DB::beginTransaction();

                if(!$post->check_belong)
                {
                    return redirect()->route('userSection.posts.index');
                }

                foreach($post->images as $image)
                {
                    if(file_exists( public_path('storage/'.$image->title) ) )
                    {
                        unlink(public_path('storage/'.$image->title));
                    }
                }
                Image::where('post_id',$post->id)->delete();
                $deleted = $post->delete();


            DB::commit();

                session()->flash('success',('Post cancellato con successo'));
                return redirect()->route('userSection.posts.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Errore nell\'eliminazione del post'
            ];
            session()->flash('error',($errors));
            return redirect()->route('userSection.posts.index');
        }

    }
}
