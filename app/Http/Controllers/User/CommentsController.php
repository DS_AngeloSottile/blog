<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentFormRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{

    public function store(CommentFormRequest $request,Post $post)
    {

        try
        {
            DB::beginTransaction();

                $user = Auth::user();

                $comment = Comment::create(
                    [
                        'post_id' => $post->id,
                        'content' => $request->content,
                        'user_id' => $user->id,
                    ]
                );

            DB::commit();

                session()->flash('success','commento creato con successo');
                return redirect()->back();
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Non è stato possibile creare il commento'
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }

    }

}
