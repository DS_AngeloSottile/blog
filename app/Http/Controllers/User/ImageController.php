<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('post.image.imageUpload');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try
        {
            DB::beginTransaction();

                if($request->hasFile('image'))
                {
                    $this->validate($request,
                    [
                        'image' =>'required|image|mimes:jpeg,png,jpg',
                    ]);
                }

                $imageName = time(). '.' .$request->image->extension();

                $request->image->move (public_path('articoli/img'),$imageName );

                $image = Image::create([

                    'title' => 'http://localhost:8000/articoli/img/'. $imageName,
                    'post_id'   => 1,

                ]);

            DB::commit();

                session()->flash('success','immagine creata con successo');
                return redirect()->back();
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Non è stato possibile aggiungere l\'immagine',
            ];
            session()->flash('error',$errors);
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
