<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{

    public function contact()
    {
        return view('components.contact');
    }

    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);

        return view('home',compact('posts'));
    }

    public function adminHome()
    {
        return view('admin.home');
    }

}
