<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class manager //per utilizzare il middleware devi andare nel kernel e aggiungerlo alla lista
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check())
        {
            return redirect('user/login');
        }
        else
        {
            $user = Auth::user();
            if($user->hasRole('manager') || $user->hasRole('admin')) //suppongo che hasRole sia una proprietà che mette laravel permission
            {
                return $next($request);
            }
            else
            {
                return redirect('home');
            }
        }

    }
}
