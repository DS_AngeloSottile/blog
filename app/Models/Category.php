<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable =
    [
        'name',
    ];

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post','category_post','category_id','post_id')->withTimestamps();
    }
}
