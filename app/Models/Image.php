<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable =
    [
        'title',
        'post_id',
    ];

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
