<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{

    protected $fillable =
    [
        'title',
        'content',
        'slug',
        'status',
        'user_id',
    ];


    protected $with=
    [
        'comments',
        'categories',
        'images',
        'user',
    ];


    public function scopeSelectDay($query,$day) // il primo parametro è già implementato. Gli scope servono per richiamare le query, in modo da non doverle scrivere piu' volte
    {
        $query = $query->withCount(['categories','comments']);

        $tomorrow = $day->clone()->addDays(1);
        return $query = $query->where('created_at','>=',$day)->where('created_at','<=',$tomorrow)->get();

    }


    public function getCheckBelongAttribute()
    {
        $id = Auth::id();

        $user = $this->user()->first();

        if($user->id == $id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public function categories()
    {
        return $this->belongsToMany('App\Models\Category','category_post','post_id','category_id')->withTimestamps();
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function comments()
    {
        return $this->hasMany('App\Models\Comment','post_id');
    }


    public function images()
    {
        return $this->hasMany('App\Models\Image','post_id');
    }




    public function getRouteKeyName()
    {
        return 'slug';
    }

}
