<?php

namespace App\Traits;

use App\Models\Image;
use App\Models\Post;

trait ImgManageTrait
{

    private function addImage( $file, Post $post)
    {

        $imageName =  time(). '.' . $file->getClientOriginalExtension();
        $file_path = 'articoli/' .$post->id. '/img';
        $uploaded_file =  $file->storeAs( $file_path,$imageName,'public');

        $image = Image::create(
            [
                'title'     => $uploaded_file,
                'post_id'   => $post->id,
            ]
        );
    }

    private function editImage( $file, Post $post,Image $image)
    {
        if(file_exists( public_path('storage/'.$post->images()->first()->title) ) )
        {
            unlink(public_path('storage/'.$post->images()->first()->title));
        }

        $imageName =  time(). '.' . $file->getClientOriginalExtension();
        $file_path = 'articoli/' .$post->id. '/img';
        $uploaded_file =  $file->storeAs( $file_path,$imageName,'public');

        $image->title = $uploaded_file;
        $image->save();
    }

}

?>
